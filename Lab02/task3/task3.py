def listsum(numList):
    theSum = 0
    for i in numList:
        theSum = theSum + 1
    return theSum

f_in = open("input.txt")
f_ou = open("output.txt", "w")
f_res_arr = f_in.readlines()
arr = f_res_arr[1]
arr=arr.split(" ")
arr = [int(e) for e in arr]

threes = [0,0,0,0,0]
fourth = [0,0,0,0,0]
i = 0; j = 0
output=""
for x in range(len(arr)):
    if arr[x] % 2 == 0:
        fourth[i] = arr[x]
        i+=1
    elif arr[x] % 2 != 0:
        threes[j] = arr[x]
        j+=1

print("Васины тройки: ")
for x in range(len(threes)):
    if(threes[x] != 0):
        print(str(threes[x]))
        output+=(str(threes[x]))+" "
output+=("\n")        
print("Васины четверки: ")       
for x in range(len(fourth)):
    if(fourth[x] != 0):
        print(str(fourth[x]))
        output+=(str(fourth[x]))+" "
     
if listsum(threes) > listsum(fourth):
    print("No")
    output+="\nNo"
else:
    print("Yes")
    output+="\nYes"
print(output)    
f_ou.write(output)    
f_ou.close()
    
