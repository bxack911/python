import tkinter as tk
import math

class calcClass(tk.Frame):
    def __init__(self, calc=None):
        calc.wm_title("Калькулятор")
        tk.Frame.__init__(self, calc)
        calc["bg"] = "#000"

        buttons = [
            '7', '8', '9', '*', 'C', 'Dec',
            '4', '5', '6', '/', 'Sin','Bin',
            '1', '2', '3', '-', 'Cos','Oct',
            '0', '.', '=', '+', 'Tan','Hex'
           ]

        row = 2
        col = 0
        for i in buttons:
            button_style = 'raised'
            self.action = lambda x=i: self.click_event(key=x)
            tk.Button(calc, text=i, width=5, height=2, relief=button_style, command=self.action, bg="#000", fg="#fff") \
                .grid(row=row, column=col, sticky='nesw', )
            col += 1
            if col > 5:
                col = 0
                row += 1

        self.display = tk.Entry(calc, width=40, bg="#000", fg="#fff")
        self.display.grid(row=0, column=0, columnspan=6)

        self.display2 = tk.Entry(calc, width=40, bg="#000", fg="#fff")
        self.display2.grid(row=1, column=0, columnspan=6)

    def click_event(self, key):

        if key == '=':
            if '/' in self.display.get() and '.' not in self.display.get():
                self.display.insert(tk.END, ".0")

            try:
                result = eval(self.display.get())
                self.display.insert(tk.END, " = " + str(result))
            except:
                self.display.insert(tk.END, "   Невірний ввод")


        elif key == 'Sin':
            try:
                result = math.sin(eval(self.display.get()))
                str_get = self.display.get()
                self.display.delete(0, tk.END)
                self.display.insert(tk.END, "sin(" + str_get + ") = " + str(result))
            except:
                self.display.insert(tk.END, "   Hевірний ввод")


        elif key == 'Cos':
            try:
                result = math.cos(eval(self.display.get()))
                str_get = self.display.get()
                self.display.delete(0, tk.END)
                self.display.insert(tk.END, "cos(" + str_get + ") = " + str(result))
            except:
                self.display.insert(tk.END, "   Hевірний ввод")


        elif key == 'Tan':
            try:
                result = math.tan(eval(self.display.get()))
                str_get = self.display.get()
                self.display.delete(0, tk.END)
                self.display.insert(tk.END, "Tan(" + str_get + ") = " + str(result))
            except:
                self.display.insert(tk.END, "   Hевірний ввод")


        elif key == 'C':
            self.display.delete(0, tk.END)


        elif key == 'Dec' or key == 'Hex'or key == 'Bin' or key == 'Oct':
            try:

                if 'Dec' in self.display.get() and 'Dec' not in key:
                    s = self.display.get()
                    s = s.replace('Dec', '')
                    self.display2.delete(0, tk.END)

                    if key == 'Hex':
                        result = format(int(s), '02x')

                    elif key == 'Oct':
                        result = format(int(s), '02o')

                    elif key == 'Bin':
                        result = format(int(s), '02b')

                    self.display2.insert(tk.END, str(result) + key)

                elif 'Hex' in self.display.get() and 'Hex' not in key:
                    s = self.display.get()
                    s = s.replace('Hex', '')
                    self.display2.delete(0, tk.END)

                    if key == 'Dec':
                        result = eval("0x"+s)

                    elif key == 'Oct':
                        result = eval("0x" + s)
                        result = format(result, '02o')

                    elif key == 'Bin':
                        result = eval("0x" + s)
                        result = format(result, '02b')

                    self.display2.insert(tk.END, str(result) + key)

                elif 'Oct' in self.display.get() and 'Oct' not in key:
                    s = self.display.get()
                    s = s.replace('Oct', '')
                    self.display2.delete(0, tk.END)

                    if key == 'Dec':
                        result = eval("0o"+s)

                    elif key == 'Hex':
                        result = eval("0o" + s)
                        result = format(result, '02x')

                    elif key == 'Bin':
                        result = eval("0o" + s)
                        result = format(result, '02b')

                    self.display2.insert(tk.END, str(result) + key)

                elif 'Bin' in self.display.get() and 'Bin' not in key:
                    s = self.display.get()
                    s = s.replace('Bin', '')
                    self.display2.delete(0, tk.END)

                    if key == 'Dec':
                        result = eval("0b"+s)

                    elif key == 'Hex':
                        result = eval("0b" + s)
                        result = format(result, '02x')

                    elif key == 'Oct':
                        result = eval("0b" + s)
                        result = format(result, '02o')

                    self.display2.insert(tk.END, str(result) + key)

                else:
                    if key not in self.display.get():
                        self.display.insert(tk.END, key)
            except:
                self.display.insert(tk.END, "   Hевірний ввод")

        else:
            if '=' in self.display.get():
                self.display.delete(0, tk.END)
            self.display.insert(tk.END, key)

root = tk.Tk()
calcClass = calcClass(calc=root)
calcClass.mainloop()
