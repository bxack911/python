import math
f=open("input.txt")
f1=open("output.txt", "w")
m=f.readlines()
x=m[0]
x=x.replace('A','1')
x=x.replace('B','2')
x=x.replace('C','3')
x=x.replace('D','4')
x=x.replace('E','5')
x=x.replace('F','6')
x=x.replace('G','7')
x=x.replace('H','8')
cap={"A", "B", "C", "D", "E", "F", "G", "H"}
if len(x)<5:
    t=("ERROR")
    print(t)
    f1.write(str(t))
    f1.close()
    exit()
x1=x[0]
x2=x[3]
y1=x[1]
y2=x[4]
y=x[2]
if x1!=cap or x2!=cap or y1>8 or y2>8 or y!="-":
    t=("ERROR")
if (abs(int(x1) - int(x2)) == 2 and abs(int(y1) - int(y2)) == 1) or (abs(int(x1) - int(x2)) == 1 and abs(int(y1) - int(y2)) == 2):
    t=("YES")
elif (abs(int(x1) - int(x2)) != 2 and abs(int(y1) - int(y2)) != 1) or (abs(int(x1) - int(x2)) != 1 and abs(int(y1) - int(y2)) != 2):
    t=("NO")
f1.write(str(t))
f.close()
f1.close()
