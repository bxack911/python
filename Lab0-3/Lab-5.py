

import tkinter as tk
import urllib.request
import json

class winClass(tk.Frame):



    def __init__(self, win=None):
        win.wm_title("Парсер")
        tk.Frame.__init__(self, win)
        win["bg"] = "#000"
        win.geometry("500x300")

        self.url = 'http://resources.finance.ua/ua/public/currency-cash.json'
        try:
            response = urllib.request.urlopen(self.url)
            body = response.read()
        except urllib.error.HTTPError:
            body = 'Connect error'
        except urllib.error.URLError:
            body = 'Url error'
        self.fromJson = json.loads(body.decode('utf-8'))
        response.close()


        self.labelDate = tk.Label(root, width=1000, justify="left", text=self.fromJson['date'], bg="#000", fg="#fff")
        self.labelDate.pack()

        self.btnRef = tk.Button(root, text="Refresh", relief="raised", bg="#000", fg="#fff",  command=self.Refresh)
        self.btnRef.pack()

        self.BankList=[]
        for data in self.fromJson['organizations']:
            self.BankList.append(data['title'])
        self.var = tk.StringVar()
        self.var.set("Selected Bank")
        self.optBank = tk.OptionMenu(root, self.var, *self.BankList, command=self.ReBank)
        self.optBank.pack()

        self.labelAddress = tk.Label(root, text="Address - ", bg="#000", fg="#fff",)
        self.labelAddress.pack()
        self.labelPhone = tk.Label(root, text="Phone - ", bg="#000", fg="#fff",)
        self.labelPhone.pack()
        self.labelCity = tk.Label(root, text="City - ", bg="#000", fg="#fff",)
        self.labelCity.pack()
        self.labelRegion = tk.Label(root, text="Region - ", bg="#000", fg="#fff",)
        self.labelRegion.pack()
        self.labelLink = tk.Label(root, text="Link - ", bg="#000", fg="#fff",)
        self.labelLink.pack()

        self.codeAlphaList = [""]
        self.var2 = tk.StringVar()
        self.var2.set("Selected $")
        self.optWin = tk.OptionMenu(root, self.var2, *self.codeAlphaList, command=self.ReWin)
        self.optWin.pack()

        self.labelAsk = tk.Label(root, text="Sale - ", bg="#000", fg="#fff",)
        self.labelAsk.pack()
        self.labelBid = tk.Label(root, text="Purshare - ", bg="#000", fg="#fff",)
        self.labelBid.pack()

    def ReBank(self,position):
        for data in self.fromJson['organizations']:
            if self.var.get()==data['title']:

                self.labelAddress['text'] = "Address - %s"%(data['address'])
                self.labelPhone['text'] = "Phone - %s"%(data['phone'])
                self.labelCity['text'] = "City - %s"%(self.fromJson['cities'][data['cityId']])
                self.labelRegion['text'] = "Region - %s"%(self.fromJson['regions'][data['regionId']])
                self.labelLink['text'] = "Link - %s"%(data['link'])

                self.optWin.destroy()
                self.labelAsk.destroy()
                self.labelBid.destroy()

                self.codeAlphaList = data['currencies'].keys()
                self.var2 = tk.StringVar()
                self.var2.set("Selected $")
                self.optWin = tk.OptionMenu(root, self.var2, *self.codeAlphaList, command=self.ReWin)
                self.optWin.pack()

                self.labelAsk = tk.Label(root, text="Sale - ", bg="#000", fg="#fff",)
                self.labelAsk.pack()
                self.labelBid = tk.Label(root, text="Purshare - ", bg="#000", fg="#fff",)
                self.labelBid.pack()

    def ReWin(self, position):
        for data in self.fromJson['organizations']:
            if self.var.get()==data['title']:

                self.labelAsk['text'] = "Sale - %s grn" % (data['currencies'][self.var2.get()]['ask'])
                self.labelBid['text'] = "Sale - %s grn" % (data['currencies'][self.var2.get()]['bid'])

    def Refresh(self):

        try:
            response = urllib.request.urlopen(self.url)
            body = response.read()
        except urllib.error.HTTPError:
            body = 'Connect error'
        except urllib.error.URLError:
            body = 'Url error'
        self.fromJson = json.loads(body.decode('utf-8'))
        response.close()

        self.labelDate['text']=self.fromJson['date']
        self.var.set("Selected Bank")
        self.labelAddress['text']="Address - "
        self.labelPhone['text']="Phone - "
        self.labelCity['text']="City - "
        self.labelRegion['text']="Region - "
        self.labelLink['text']="Link - "

        self.optWin.destroy()
        self.labelAsk.destroy()
        self.labelBid.destroy()

        self.codeAlphaList = [""]
        self.var2 = tk.StringVar()
        self.var2.set("Select $")
        self.optWin = tk.OptionMenu(root, self.var2, *self.codeAlphaList, command=self.ReWin)
        self.optWin.pack()

        self.labelAsk = tk.Label(root, text="Sale - ", bg="#000", fg="#fff",)
        self.labelAsk.pack()
        self.labelBid = tk.Label(root, text="Purshare - ", bg="#000", fg="#fff",)
        self.labelBid.pack()

root = tk.Tk()
winClass = winClass(win=root)
winClass.mainloop()
