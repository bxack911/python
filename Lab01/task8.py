import math
t = str(input("Введите t: "))
if t == "треугольник":
    a = int(input("Введите A: "))
    b = int(input("Введите B: "))
    c = int(input("Введите C: "))
    p = (a + b + c)/2
    S = math.sqrt(p * (p - a) * (p - b) * (p - c))
    print(S)
elif t == "прямоугольник":
    a = int(input("Введите A: "))
    b = int(input("Введите B: "))
    S = a * b
    print(S)
elif t == "круг":
    r = int(input("Введите радиус: "))
    S = 3.14 * r ** 2
    print(S)
