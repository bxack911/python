# -*- coding: utf-8 -*-

import socket
from tkinter import *

tk=Tk()

s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
s.bind(('',13))

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST,1)


name = StringVar()
name.set('')

nameLabel = StringVar()
nameLabel.set("Ваше ім'я")

text = StringVar()
text.set('')

textLabel = StringVar()
textLabel.set('Текст повідомлення')

buttonTxt = StringVar()
buttonTxt.set('Відправити')

tk.title('Чат. Лаб-03')
tk.geometry('400x300')

log = Text(tk)
nick = Entry(tk, textvariable=name)
nickLabel = Label(tk, text="Ім'я відправника", fg="blue")
msg = Entry(tk, textvariable=text)
msgLabel = Label(tk, text="Текст повідомлення", fg="blue")

def callback():
    message = '{name}:{text}'.format(name=name.get(), text=text.get())
    bmessage = message.encode()
    sock.sendto(bmessage, ('255.255.255.255', 13))
    text.set('')
    
button = Button(tk, textvariable=buttonTxt, fg='#fff', bg='#000', command=callback)

button.pack(side='bottom', fill='x', expand='true')

msg.pack(side='bottom', fill='x', expand='true')
msgLabel.pack(side='bottom', fill='x', expand='true')

nick.pack(side='bottom', fill='x', expand='true')
nickLabel.pack(side='bottom', fill='x', expand='true')

log.pack(side='top', fill='both',expand='true')

nick.focus_set()

def loopproc():
    log.see(END)
    s.setblocking(False)
    try:
        bmessage = s.recv(256)
        message = bmessage.decode()
        log.insert(END,message+'\n')
    except:
        tk.after(1,loopproc)
        return
    tk.after(1,loopproc)
    return

def sendproc(event):
    message = '{name}:{text}'.format(name=name.get(), text=text.get())
    bmessage = message.encode()
    sock.sendto(bmessage, ('255.255.255.255', 13))
    text.set('')  

msg.bind('<Return>', sendproc)
msg.focus_set()
tk.after(1,loopproc)
tk.mainloop()
