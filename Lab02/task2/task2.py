f_in = open("input.txt")
f_ou = open("output.txt", "w")
f_res_arr = f_in.readlines()
x = int(f_res_arr[0])

if x == 1:
    print("198")
elif x == 2:
    print("297")
elif x == 3:
    print("396")
elif x == 4:
    print("495")
elif x == 5:
    print("594")
elif x == 6:
    print("693")
elif x == 7:
    print("792")
elif x == 8:
    print("891")
elif x == 9:
    print("990")
f_ou.write(str(x))
f_ou.close()
    
